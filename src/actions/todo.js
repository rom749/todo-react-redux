import keyMirror from 'keymirror';

let nextTodoId = 0;

const
	addTodo = title => ({
		type: types.CREATE_TODO,
		todo: {
			completed: false,
			edit: false,
			id: nextTodoId++,
			title
		}
	}),

	deleteTodo = id => ({
		id,
		type: types.DELETE_TODO,
	}),

	toggleDone = id => ({
		id,
		type: types.TOGGLE_DONE,
	}),

	toggleEdit = id => ({
		id,
		type: types.TOGGLE_EDIT,
	}),

	updateTodo = params => ({
		payload: {
			id   : params.id,
			title: params.title,
		},
		type: types.UPDATE_TODO,
	}),

	types = keyMirror({
		CREATE_TODO: null,
		DELETE_TODO: null,
		EDIT_TODO: null,
		TOGGLE_DONE: null,
		TOGGLE_EDIT: null,
		UPDATE_TODO: null,
		SET_VISIBILITY_FILTER: null,
	});

export default {addTodo, deleteTodo, toggleDone, toggleEdit, updateTodo, types};
