import keyMirror from 'keymirror';

const
	setFilter = filter => ({
		type: types.SET_FILTER,
		filter
	}),

	types = keyMirror({
		SET_FILTER: null,
	}),

	filters = keyMirror({
		SHOW_ACTIVE: null,
		SHOW_ALL: null,
		SHOW_COMPLETED: null,
	});

export default {setFilter, types, filters};
