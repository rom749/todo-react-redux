import _ from 'lodash';
import actions from '../actions/todo';
import update from 'immutability-helper';

const
	todoReducer = (state = [], action) => {
		switch (action.type) {
			case actions.types.CREATE_TODO:
				return update(state, {$unshift: [action.todo]});

			case actions.types.DELETE_TODO:
				return _.reject(state, {id: action.id});

			case actions.types.TOGGLE_DONE: {
				const
					index = _.findIndex(state, {id: action.id});

				return update(state, {[index]: {completed: {$set: !state[index].completed}}});
			}

			case actions.types.TOGGLE_EDIT: {
				const
					index = _.findIndex(state, {id: action.id});

				return update(state, {[index]: {edit: {$set: !state[index].edit}}});
			}

			case actions.types.UPDATE_TODO: {
				const
					index = _.findIndex(state, {id: action.payload.id});

				return update(state, {[index]: {$merge: {title: action.payload.title, edit: false}}});
			}

			default:
				return state;
		}
};

export default todoReducer;
