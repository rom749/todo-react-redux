import actions from '../actions/filters';

const
	filterReducer = (state = actions.filters.SHOW_ALL, action) => {
		switch (action.type) {
			case actions.types.SET_FILTER:
				return action.filter;

			default:
				return state;
		}
};

export default filterReducer;
