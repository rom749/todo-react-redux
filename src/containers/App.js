import _ from 'lodash';
import filtersActions from '../actions/filters';
import {connect} from 'react-redux';
import todoActions from '../actions/todo';
import Todo from '../components/Todo';

export default connect(
	state => ({
		todos: state.filter === filtersActions.filters.SHOW_ALL
			? state.todos
			: _.filter(state.todos, {completed: state.filter === filtersActions.filters.SHOW_COMPLETED}),

		todosIsNoEmpty: !_.isEmpty(state.todos),
	}),

	{
		deleteTodo  : todoActions.deleteTodo,
		toggleDone  : todoActions.toggleDone,
		toggleEdit  : todoActions.toggleEdit,
	}
)(Todo);
