import Filters from '../components/Filters'
import filtersActions from '../actions/filters';
import {connect} from 'react-redux';

export default connect(
	state => ({
		filterValue: state.filter,
	}),

	{
		changeFilter: filtersActions.setFilter,
	}
)(Filters);
