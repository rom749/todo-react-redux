import actions from '../actions/todo';
import {connect} from 'react-redux';
import Form from '../components/Form'
import React from 'react';

class FormContainer extends React.Component {
	constructor(props) {
		super(props);
		this.isItemForm = Boolean(this.props.todo);
		this.state = {value: this.isItemForm ? this.props.todo.title : ''};
	}

	handleChange = e => this.setState({value: e.target.value});

	handleSubmit = e => {
		e.preventDefault();

		if (!this.state.value.trim())
			return;

		if(this.isItemForm)
			this.props.updateTodo({
				id: this.props.todo.id,
				title: this.state.value,
			});

		else{
			this.props.addTodo(this.state.value);
			this.setState({value: ''});
		}
	};

	render() {
		return (
			<Form
				onChange={this.handleChange}
				onSubmit={this.handleSubmit}
				isItemForm={this.isItemForm}
				style={{fontSize: this.isItemForm ? 20 : 30}}
				value={this.state.value}
			/>
		);
	}
}

export default connect(
	null,

	{
		addTodo: actions.addTodo,
		updateTodo: actions.updateTodo,
	}
)(FormContainer);
