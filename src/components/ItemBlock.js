import React from 'react';
import styled from 'styled-components';

const Block = styled.div`
	border-radius: 2px;
	box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);
	padding: 15px;
	margin-bottom: 20px;
`;

class ItemBlock extends React.Component {
	shouldComponentUpdate(nextProps) {
		return (nextProps.todo.completed !== this.props.todo.completed ||
			nextProps.todo.edit !== this.props.todo.edit
		);
	}

	render() {
		return <Block style={this.props.style}>
			{this.props.children}
		</Block>
	}
}

export default ItemBlock;
