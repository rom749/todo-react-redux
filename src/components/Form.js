import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import React from 'react';

const
	Form = props => <Grid item xs={12}>
		<form onSubmit={props.onSubmit}>
			<Input
				fullWidth
				onChange={props.onChange}
				placeholder={props.isItemForm ? '' : 'Add todo!'}
				style={props.style}
				value={props.value}
			/>
		</form>
	</Grid>;

export default Form;
