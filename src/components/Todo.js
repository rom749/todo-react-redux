import _ from 'lodash';
import EditedItem from '../components/EditedItem'
import FilterContainer from '../containers/FilterContainer';
import FormContainer from '../containers/FormContainer';
import Grid from '@material-ui/core/Grid';
import Item from './Item';
import ItemBlock from './ItemBlock';
import React from 'react';
import {withStyles} from '@material-ui/core/styles';

const
	Todo = props =>  <div className={props.classes.root}>
		<Grid container spacing={24}>
			<Grid item xs={12}>
				<h3>Todo React Redux</h3>
			</Grid>
			<FormContainer />
			{props.todosIsNoEmpty && <FilterContainer />}

			<Grid item xs={12}>
				{_.map(props.todos, todo => <ItemBlock
						key={todo.id}
						todo={todo}
						style={todo.completed ? {backgroundColor: '#F1F8E9'} : {}}
					>

						{todo.edit
							? <EditedItem
								todo={todo}
								toggleEdit={props.toggleEdit}
							/>

							: <Item
								deleteTodo={props.deleteTodo}
								todo={todo}
								toggleDone={props.toggleDone}
								toggleEdit={props.toggleEdit}
							/>
						}
					</ItemBlock>
				)}
			</Grid>
		</Grid>
	</div>,

	styles = () => ({
		root: {
			flexGrow: 1,
			margin: 'auto',
			width: 800,
		}
	});

export default withStyles(styles)(Todo);
