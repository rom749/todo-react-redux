import ClearIcon from '@material-ui/icons/Clear';
import FormContainer from '../containers/FormContainer';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import React from 'react';

const
	EditedItem = props => <Grid container>
			<Grid item xs={11}>
				<FormContainer todo={props.todo} />
			</Grid>

			<Grid item xs={1}>
				<IconButton
					aria-label="chancel"
					color="secondary"
					onClick={() => props.toggleEdit(props.todo.id)}
					mini="true"
					variant="fab"
				>

					<ClearIcon />
				</IconButton>
			</Grid>
		</Grid>;

export default EditedItem;
