import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from '@material-ui/icons/Done';
import EditIcon from '@material-ui/icons/Edit';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import React from 'react';

class Item extends React.PureComponent {
	render() {
		return <Grid container>
			<Grid item xs={1}>
				<IconButton
					aria-label="done"
					onClick={() => this.props.toggleDone(this.props.todo.id)}
					mini="true"
					variant="fab"
				>

					<DoneIcon style={this.props.todo.completed ? {color: '#8BC34A'} : {}}/>
				</IconButton>
			</Grid>

			<Grid item xs={9}>
				<h4 style={{marginTop: 15}}>{this.props.todo.title}</h4>
			</Grid>

			<Grid item xs={2}>
				<IconButton
					aria-label="edit"
					color="primary"
					onClick={() => this.props.toggleEdit(this.props.todo.id)}
					mini="true"
					variant="fab"
				>

					<EditIcon/>
				</IconButton>

				<IconButton
					aria-label="delete"
					color="secondary"
					onClick={() => this.props.deleteTodo(this.props.todo.id)}
					mini="true"
					variant="fab"
				>

					<DeleteIcon/>
				</IconButton>
			</Grid>
		</Grid>
	}
}

export default Item;
