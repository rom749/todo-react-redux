import actions from '../actions/filters'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import React from 'react';

const
	Filters = props => <Grid item xs={12}>
			<RadioGroup
				aria-label="filters"
				name="filters"
				onChange={(e, v) => props.changeFilter(v)}
				row
				value={props.filterValue}
			>

				<FormControlLabel value={actions.filters.SHOW_ALL} control={<Radio />} label="View All" />
				<FormControlLabel value={actions.filters.SHOW_ACTIVE} control={<Radio />} label="Active" />
				<FormControlLabel value={actions.filters.SHOW_COMPLETED} control={<Radio />} label="Completed" />
			</RadioGroup>
		</Grid>;

export default Filters;
