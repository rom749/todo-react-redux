import App from './containers/App';
import {createStore} from 'redux';
import { Provider } from 'react-redux';
import React from 'react';
import reducers from './reducers';
import {render} from 'react-dom';

const
	store = createStore(reducers);

render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);
